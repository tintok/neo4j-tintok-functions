import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.30"
    //id("com.github.johnrengelman.shadow") version "6.1.0"
}

group = "de.iptk.groupj"
version = "$version"
java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    // Neo4j
    implementation(group = "org.neo4j", name = "neo4j", version = "4.2.3")

    // Tests
    // Embedded neo4j database
    testImplementation(group = "org.neo4j.test", name = "neo4j-harness", version = "4.2.3")
    testImplementation(group = "org.neo4j.driver", name = "neo4j-java-driver", version = "4.2.3")

    // JUnit and asserting
    testImplementation(platform("org.junit:junit-bom:5.7.1"))
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter")
    testImplementation(group = "io.kotest", name = "kotest-assertions-core", version = "4.4.3")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version
            )
        )
    }
}
