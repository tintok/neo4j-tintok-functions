package de.iptk.groupj

import org.neo4j.graphdb.Node
import org.neo4j.procedure.Description
import org.neo4j.procedure.Name
import org.neo4j.procedure.UserFunction
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

const val DISTANCE_BONUS_MODIFIER = 1
const val DISTANCE_BONUS_LIMIT = 5000
const val AGE_BONUS_MODIFIER = 1
const val AGE_BONUS_LIMIT = 24 * 60 * 60 // 24 hours

class PostScore {
    @UserFunction
    @Description(
        "de.iptk.groupj.postScore(post: Node, likes: Integer, dislikes: Integer, distance: Float, " +
                "ageBonusModifier: Float = 1, ageBonusLimit: Integer = 5000, distanceBonusModifier: Float = 1, " +
                "distanceBonus: Integer = 86400) - " +
                "calculates a score for a post based on the amount of likes and dislikes, the given distance and the " +
                "age of the post. A 'created' property of type DATETIME must be present on the post node."
    )
    fun postScore(
        @Name("post") post: Node?,
        @Name("likes") likes: Long?,
        @Name("dislikes") dislikes: Long?,
        @Name("distance") distance: Double?,
        @Name("ageBonusModifier", defaultValue = "$AGE_BONUS_MODIFIER") ageBonusModifier: Double?,
        @Name("ageBonusLimit", defaultValue = "$AGE_BONUS_LIMIT") ageBonusLimit: Long?,
        @Name("distanceBonusModifier", defaultValue = "$DISTANCE_BONUS_MODIFIER") distanceBonusModifier: Double?,
        @Name("distanceBonusLimit", defaultValue = "$DISTANCE_BONUS_LIMIT") distanceBonusLimit: Long?
    ): Double? {
        if (post == null || likes == null || dislikes == null || distance == null || ageBonusModifier == null ||
            ageBonusLimit == null || distanceBonusModifier == null || distanceBonusLimit == null
        )
            return null
        if (!post.hasProperty("created"))
            return null
        val created = post.getProperty("created")
        if (created !is ZonedDateTime)
            return null

        val rating = likes - dislikes + 1           // add one (publisher likes own post)
        val age = ChronoUnit.SECONDS.between(created, ZonedDateTime.now()).toDouble()
        val x = if (rating <= 1) 1.0 else rating.toDouble()

        // promote posts near location
        val distanceScore: Double = if (distance >= distanceBonusLimit) 1.0
        else -(distance / (distanceBonusLimit / distanceBonusModifier)) + distanceBonusModifier + 1
        // promote more recent posts
        val ageScore: Double = if (age >= ageBonusLimit) 1.0
        else -(age / (ageBonusLimit / ageBonusModifier)) + ageBonusModifier + 1
        // rating score promotes posts with more likes
        val ratingScore: Double = Math.log10(x) + 1
        return distanceScore * ratingScore * ageScore
    }
}