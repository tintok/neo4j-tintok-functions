package de.iptk.groupj

import io.kotest.assertions.throwables.shouldThrowAny
import io.kotest.matchers.collections.shouldBeSortedWith
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.*
import org.neo4j.driver.Config
import org.neo4j.driver.Driver
import org.neo4j.driver.GraphDatabase
import org.neo4j.driver.Record
import org.neo4j.harness.Neo4j
import org.neo4j.harness.Neo4jBuilders
import java.lang.IllegalArgumentException
import java.time.ZonedDateTime

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PostScoreTest {
    private val driverConfig: Config = Config.builder().withoutEncryption().build()
    private lateinit var driver: Driver
    private lateinit var embeddedDatabaseServer: Neo4j

    @BeforeAll
    fun initializeNeo4j() {
        embeddedDatabaseServer = Neo4jBuilders.newInProcessBuilder()
            .withDisabledServer()
            .withFunction(PostScore::class.java)
            .build()
        driver = GraphDatabase.driver(embeddedDatabaseServer.boltURI(), driverConfig)
    }

    @AfterAll
    fun closeDriver() {
        driver.close()
        embeddedDatabaseServer.close()
    }

    @AfterEach
    fun cleanDb() {
        driver.session().use { session -> session.run("MATCH (n) DETACH DELETE n") }
    }

    @Test
    internal fun `single post score`() {
        driver.session().use { session ->
            // create single post
            session.run("CREATE (:Post {created: datetime('${ZonedDateTime.now()}')})")

            // test single result
            val result = session.run(
                """
                MATCH (p:Post) 
                RETURN de.iptk.groupj.postScore(p, 0, 0, 0) AS score
                """.trimIndent()
            ).single()

            result.size() shouldBe 1
            val score = result[0].asDouble()
            // post is created now (ageScore=2), with no distance (distanceScore=2)
            score shouldBe 4.0
        }
    }

    @Test
    internal fun `single post score with age modifier`() {
        driver.session().use { session ->
            // create single post
            session.run("CREATE (:Post {created: datetime('${ZonedDateTime.now()}')})")

            // test single result
            val result = session.run(
                """
                MATCH (p:Post) 
                RETURN de.iptk.groupj.postScore(p, 0, 0, 0, 0.5, $AGE_BONUS_LIMIT, $DISTANCE_BONUS_MODIFIER, 
                $DISTANCE_BONUS_LIMIT) AS score
                """.trimIndent()
            ).single()

            result.size() shouldBe 1
            val score = result[0].asDouble()
            // post is created now (ageScore=1.5), with no distance (distanceScore=2)
            score shouldBe 3.0
        }
    }

    @Test
    internal fun `single post score with distance modifier`() {
        driver.session().use { session ->
            // create single post
            session.run("CREATE (:Post {created: datetime('${ZonedDateTime.now()}')})")

            // test single result
            val result = session.run(
                """
                MATCH (p:Post) 
                RETURN de.iptk.groupj.postScore(p, 0, 0, 0, $AGE_BONUS_MODIFIER, $AGE_BONUS_LIMIT, 0.5,
                $DISTANCE_BONUS_LIMIT) AS score
                """.trimIndent()
            ).single()

            result.size() shouldBe 1
            val score = result[0].asDouble()
            // post is created now (ageScore=2), with no distance (distanceScore=1.5)
            score shouldBe 3.0
        }
    }

    @Test
    internal fun `no created property`() {
        driver.session().use { session ->
            // create post without created property
            session.run("CREATE (:Post)")

            // test single result
            val result = session.run(
                """
                MATCH (p:Post) 
                RETURN de.iptk.groupj.postScore(p, 0, 0, 0) AS score
                """.trimIndent()
            ).single()

            result.size() shouldBe 1
            result[0].isNull shouldBe true
        }
    }

    @Test
    internal fun `invalid created property`() {
        driver.session().use { session ->
            // create post with invalid created property
            session.run("CREATE (:Post {created: 'this is not a datetime object'})")

            // test single result
            val result = session.run(
                """
                MATCH (p:Post) 
                RETURN de.iptk.groupj.postScore(p, 0, 0, 0) AS score
                """.trimIndent()
            ).single()

            result.size() shouldBe 1
            result[0].isNull shouldBe true
        }
    }

    @Test
    internal fun `invalid arguments`() {
        driver.session().use { session ->
            // create single post
            session.run("CREATE (:Post {created: datetime('${ZonedDateTime.now()}')})")

            shouldThrowAny {
                session.run(
                    """
                    MATCH (p:Post) 
                    RETURN de.iptk.groupj.postScore('', 0, 0, 0) AS score
                    """.trimIndent()
                ).single()
            }

            shouldThrowAny {
                session.run(
                    """
                    MATCH (p:Post) 
                    RETURN de.iptk.groupj.postScore(p, '', 0, 0) AS score
                    """.trimIndent()
                ).single()
            }

            shouldThrowAny {
                session.run(
                    """
                    MATCH (p:Post) 
                    RETURN de.iptk.groupj.postScore(p, 0, '', 0) AS score
                    """.trimIndent()
                ).single()
            }

            shouldThrowAny {
                session.run(
                    """
                    MATCH (p:Post) 
                    RETURN de.iptk.groupj.postScore(p, 0, 0, '') AS score
                    """.trimIndent()
                ).single()
            }
        }
    }

    @Test
    internal fun `sort by rating`() {
        driver.session().use { session ->
            // create 10 posts at same time, with increasing amount of likes
            val posts = 10
            val now = ZonedDateTime.now()
            for (i in 1..posts) {
                session.run("CREATE (:Post {likes: $i, created: datetime('$now')})")
            }

            // test score should be increasing
            val result = session.run(
                """
                MATCH (p:Post)
                WITH p, de.iptk.groupj.postScore(p, p.likes, 0, 0) AS score
                ORDER BY p.likes DESC
                RETURN score, p
                """.trimIndent()
            ).list()

            result.size shouldBe posts
            result shouldBeSortedWith ScoreComparator()
        }
    }

    @Test
    internal fun `no penalty for negative rating`() {
        driver.session().use { session ->
            // create 10 posts at same time, with increasing amount of dislikes
            val posts = 10
            val now = ZonedDateTime.now()
            for (i in 1..posts) {
                session.run("CREATE (:Post {dislikes: $i, created: datetime('$now')})")
            }

            // test score should be increasing
            val result = session.run(
                """
                MATCH (p:Post)
                RETURN de.iptk.groupj.postScore(p, 0, p.dislikes, 0) AS score
                """.trimIndent()
            ).list()

            result.size shouldBe posts
            result.map { it[0].asDouble() }.forEach {
                // just distance and time bonus
                it shouldBe 4.0
            }
        }
    }

    @Test
    internal fun `sort by distance`() {
        driver.session().use { session ->
            // create 10 posts at same time, with increasing distance
            val posts = 10
            val now = ZonedDateTime.now()
            for (i in 1..posts) {
                session.run("CREATE (:Post {distance: ${i * 1000}, created: datetime('$now')})")
            }

            // test score should be increasing
            val result = session.run(
                """
                MATCH (p:Post)
                WITH p, de.iptk.groupj.postScore(p, 0, 0, p.distance) AS score
                ORDER BY p.distance ASC
                RETURN score, p
                """.trimIndent()
            ).list()

            result.size shouldBe posts
            result shouldBeSortedWith ScoreComparator()
        }
    }

    @Test
    internal fun `no distance bonus after limit`() {
        driver.session().use { session ->
            // create 10 posts at same time, with increasing distance
            val posts = 10
            val now = ZonedDateTime.now()
            for (i in 1..posts) {
                val distance = DISTANCE_BONUS_LIMIT + i
                session.run("CREATE (:Post {distance: $distance, created: datetime('$now')})")
            }

            // test score should not be increasing
            val result = session.run(
                """
                MATCH (p:Post)
                RETURN de.iptk.groupj.postScore(p, 0, 0, p.distance) AS score
                """.trimIndent()
            ).list()

            result.size shouldBe posts
            result.map { it[0].asDouble() }.forEach {
                // no distance bonus, just time bonus
                it shouldBe 2.0
            }
        }
    }

    @Test
    internal fun `sort by time`() {
        driver.session().use { session ->
            // create 10 posts at decreasing time
            val posts = 10
            val now = ZonedDateTime.now()
            for (i in 1..posts) {
                val created = now.minusHours(i.toLong())
                session.run("CREATE (:Post {created: datetime('$created')})")
            }

            // test score should be increasing
            val result = session.run(
                """
                MATCH (p:Post)
                WITH p, de.iptk.groupj.postScore(p, 0, 0, 0) AS score
                ORDER BY p.created DESC
                RETURN score, p
                """.trimIndent()
            ).list()

            result.size shouldBe posts
            result shouldBeSortedWith ScoreComparator()
        }
    }

    @Test
    internal fun `no time bonus after limit`() {
        driver.session().use { session ->
            // create 10 posts at decreasing time after the limit
            val posts = 10
            val now = ZonedDateTime.now().minusHours(AGE_BONUS_LIMIT.toLong())
            for (i in 1..posts) {
                val created = now.minusHours(i.toLong())
                session.run("CREATE (:Post {created: datetime('$created')})")
            }

            // test score should not be increasing
            val result = session.run(
                """
                MATCH (p:Post)
                RETURN de.iptk.groupj.postScore(p, 0, 0, 0) AS score
                """.trimIndent()
            ).list()

            result.size shouldBe posts
            result.map { it[0].asDouble() }.forEach {
                // no time bonus, just distance bonus
                it shouldBe 2.0
            }
        }
    }
}

class ScoreComparator : Comparator<Record> {
    override fun compare(r1: Record?, r2: Record?): Int {
        if (r1 == null || r2 == null)
            throw IllegalArgumentException("Records cannot be null")
        if (r1.size() == 0 || r2.size() == 0)
            throw IllegalArgumentException("Records should hold at least one value")
        // scale up to actually compare differences up to 0.0001
        return ((r2[0].asDouble() - r1[0].asDouble()) * 10000).toInt()
    }
}
